﻿using System;
namespace GameTools
{
    public class GameEngine
    {
        protected MyFirstProgram.MatrixRepresentation matrix = new();
        private ConsoleColor _backgroundConsoleColor;
        public ConsoleColor BackgroundConsoleColor
        {
            get { return _backgroundConsoleColor; }
            set
            {
                if (ConsoleColor.White != value || ConsoleColor.Black != value) _backgroundConsoleColor = value;
                else
                {
                    _backgroundConsoleColor = ConsoleColor.Gray;
                    throw new ArgumentException($"Console color {value} not recomended. Set by default");
                }
            }
        }

        private int time2liveframe;
        private float _frameRate;
        public float FrameRate
        {
            get { return _frameRate; }
            set
            {
                _frameRate = (value < 0f) ? value * (-1f) : value;
            }
        }

        public int Frames { get; set; }

        private ConsoleKeyInfo cki;
        private bool engineSwitch;

        public GameEngine()
        {
            InitGame();
            UpdateGame();
            CloseGame();
        }

        private void InitGame()
        {
            Frames = 0;
            engineSwitch = true;
            this._frameRate = (_frameRate <= 0) ? 12 : _frameRate;
            time2liveframe = (int)((1 / _frameRate) * 1000);
            CleanFrame();
            Console.BackgroundColor = _backgroundConsoleColor;
            Console.WriteLine($"Game Initiation             Render data: Framerate: {_frameRate} || TimeToRefresh:{time2liveframe}");
            Start();
            System.Threading.Thread.Sleep(2000);
        }

        private void UpdateGame()
        {
            do
            {
                while (Console.KeyAvailable == false)
                {
                    CleanFrame();
                    Console.WriteLine($"Frame number {Frames}");
                    Update();
                    RefreshFrame();
                    CheckKeyboard4Engine();
                    Frames++;
                }
                cki = Console.ReadKey(true);
            } while (engineSwitch);
        }

        private void ListenKeyboard()
        {
            cki = Console.ReadKey();
        }

        private void CheckKeyboard4Engine()
        {
            engineSwitch = (cki.Key != ConsoleKey.Escape);
        }

        private void RefreshFrame()
        {
            System.Threading.Thread.Sleep(time2liveframe);
        }

        private void CloseGame()
        {
            Console.WriteLine("You pressed the '{0}' key.", cki.Key);
            Exit();
            Console.WriteLine(" Game Over. Closing game");
        }

        private void CleanFrame()
        {
            Console.Clear();
            Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop);
        }

        protected void Start()
        {
            matrix.clippingMatrix(matrix.blankMatrix());

        }

        protected void Update()
        {
            matrix.printMatrix();
            for (int i = matrix.TheMatrix.GetLength(0) - 1; i > 0; i--)
            {
                for (int j = matrix.TheMatrix.GetLength(1) - 1; j >= 0; j--)
                {
                    char temp = matrix.TheMatrix[i - 1, j];
                    matrix.TheMatrix[i - 1, j] = matrix.TheMatrix[i, j];
                    matrix.TheMatrix[i, j] = temp;
                }
            }

            Console.WriteLine("PRESS ALFABETICAL CHAR TO RESET");
            Console.WriteLine("PRESS NUMBER 9 TO START");
            Console.WriteLine("PRESS ANY OTHER NUMBER TO START INDEPENDENT COLUMNS");

            Random rnd = new();

            if (!char.IsDigit(cki.KeyChar))
            {
                for (int i = matrix.TheMatrix.GetLength(0) - 1; i >= 0; i--)
                {
                    for (int j = matrix.TheMatrix.GetLength(1) - 1; j >= 0; j--)
                    {
                        matrix.TheMatrix[i, j] = '0';
                    }
                }
            }
            else if (cki.KeyChar.CompareTo('9') == 0)
            {
                for (int i = 0; i < matrix.TheMatrix.GetLength(0); i++)
                {
                    matrix.TheMatrix[0, i] = (char)('a' + rnd.Next(0, 26));
                }
            }
            else if (char.IsDigit(cki.KeyChar) && int.Parse(cki.KeyChar.ToString()) < matrix.TheMatrix.GetLength(0))
            {
                matrix.TheMatrix[0, int.Parse(cki.KeyChar.ToString())] = (char)('a' + rnd.Next(0, 26));
            }

        }
        protected void Exit()
        {
        }
    }
}


public class ConsoleSpinner
{
    int counter;
    public ConsoleSpinner()
    {
        counter = 0;
    }
    public void Turn()
    {
        counter++;
        switch (counter % 4)
        {
            case 0: Console.Write("/"); break;
            case 1: Console.Write("-"); break;
            case 2: Console.Write("\\"); break;
            case 3: Console.Write("|"); break;
        }
    }
}
